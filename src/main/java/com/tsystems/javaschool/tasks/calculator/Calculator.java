package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0)
            return null;
        List<String> numbers = new ArrayList<>();
        LinkedList<String> operators = new LinkedList<>();
        String current;
        StringTokenizer tokenizer = new StringTokenizer(statement, "()+-*/", true);
        while (tokenizer.hasMoreTokens()) {
            current = tokenizer.nextToken();
            if (current.equals("(")) operators.add("(");
            else if (current.equals(")")) {
                while (!operators.getLast().equals("(")) {
                    numbers.add(operators.pollLast());
                    if (operators.isEmpty()) {
                        return null;
                    }
                }
                operators.pollLast();
            } else if (isOperator(current)) {
                if (!tokenizer.hasMoreTokens()) {
                    return null;
                }
                while (!operators.isEmpty() && priorityOf(current) <= priorityOf(operators.peekLast())) {
                    numbers.add(operators.pollLast());
                }
                operators.add(current);
            } else {
                numbers.add(current);
            }
        }
        while (!operators.isEmpty()) {
            if (isOperator(operators.peekLast())) numbers.add(operators.pollLast());
            else {
                return null;
            }
        }
        return calculate(numbers);
    }

    private String calculate(List<String> numbers) {
        LinkedList<Double> result = new LinkedList<>();
        Double a, b;
        for (String i : numbers) {
            switch (i) {
                case "+":
                    try {
                        result.add(result.pollLast() + result.pollLast());
                    } catch (NullPointerException e){
                        return null;
                    }
                    break;
                case "-":
                    try {
                        a = result.pollLast();
                        b = result.pollLast();
                        result.add(b - a);
                    } catch (NullPointerException e){
                        return null;
                    }
                    break;
                case "*":
                    try {
                        result.add(result.pollLast() * result.pollLast());
                    } catch (NullPointerException e){
                        return null;
                    }
                    break;
                case "/":
                    try {
                        a = result.pollLast();
                        b = result.pollLast();
                        if (a == 0) return null;
                        result.add(b / a);
                    } catch (NullPointerException e){
                        return null;
                    }
                    break;
                default:
                    try {
                        result.add(Double.parseDouble(i));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    break;
            }
        }
        return result.pop().toString().replaceAll("\\.(.*?)0+$", ".$1").replaceAll("\\.$", "");
    }

    private boolean isOperator(String c) {
        return c.equals("+") || c.equals("-") || c.equals("*") || c.equals("/");
    }

    private int priorityOf(String operator) {
        switch (operator) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            default:
                return -1;
        }
    }

}
