package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException | OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }

        int listSize = inputNumbers.size(), n, m, border = 0, count = 1;
        while (listSize > 0) {
            listSize -= count;
            count++;
        }
        if (listSize != 0) {
            throw new CannotBuildPyramidException();
        }
        n = --count;
        m = count + count - 1;
        listSize = inputNumbers.size();
        int[][] result = new int[n][m];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = m - 1; j >= border; j = j - 2) {
                result[i][j] = inputNumbers.get(--listSize);
            }
            --m;
            ++border;
        }
        return result;
    }
}
