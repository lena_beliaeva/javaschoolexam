package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        List temp = new ArrayList();

        if (x == null || y == null){
            throw new IllegalArgumentException();
        }

        if (x.size() > y.size()){
            return false;
        }

        for (Object o : y) {
            if (x.contains(o)) {
                temp.add(o);
            }
        }

        return temp.equals(x);
    }
}
